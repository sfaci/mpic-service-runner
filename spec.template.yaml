openapi: 3.0.0
info:
  version: 0.1
  title: Metrics Platform Instrument Configurator
  description: A service to configure instruments for Metrics Platform
  termsOfService: https://wikimediafoundation.org/wiki/Terms_of_Use
  contact:
    name: Data Products Team
    url: https://m.mediawiki.org/wiki/Data_Platform_Engineering/Data_Products
  license:
    name: Apache2
    url: http://www.apache.org/licenses/LICENSE-2.0

paths:
  # from routes/root.js
  /robots.txt:
    get:
      tags:
        - Root
        - Robots
      description: Gets robots.txt
      responses:
        200:
          description: Success
        default:
          description: Error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/problem"
      x-amples:
        - title: robots.txt check
          request: {}
          response:
            status: 200
  /:
    get:
      tags:
        - Root
      description: The root service end-point
      responses:
        200:
          description: Success
        default:
          description: Error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/problem"
      x-amples:
        - title: root with no query params
          request: {}
          response:
            status: 404
        - title: spec from root
          request:
            query:
              spec: true
          response:
            status: 200
        - title: doc from root
          request:
            query:
              doc: true
          response:
            status: 200
        - title: root with wrong query param
          request:
            query:
              fooo: true
          response:
            status: 404
  # from routes/info.js
  /_info:
    get:
      tags:
        - Service information
      description: Gets information about the service
      responses:
        200:
          description: OK
      x-amples:
        - title: retrieve service info
          request: {}
          response:
            status: 200
            headers:
              content-type: application/json
            body:
              name: /.+/
              description: /.+/
              version: /.+/
              home: /.+/
  /_info/name:
    get:
      tags:
        - Service information
        - Service name
      description: Gets the name of the service
      responses:
        200:
          description: OK
      x-amples:
        - title: retrieve service name
          request: {}
          response:
            status: 200
            headers:
              content-type: application/json
            body:
              name: /.+/
  /_info/version:
    get:
      tags:
        - Service information
        - Service version
      description: Gets the running version of the service
      responses:
        200:
          description: OK
      x-amples:
        - title: retrieve service version
          request: {}
          response:
            status: 200
            headers:
              content-type: application/json
            body:
              version: /.+/
  /_info/home:
    get:
      tags:
        - Service information
        - Service homepage
      description: Redirects to the home page
      responses:
        301:
          description: Redirect
      x-amples:
        - title: redirect to the home page
          request: {}
          response:
            status: 301
  # from routes/ex.js
  /ex/err/array:
    get:
      tags:
        - Example
        - Error
        - Internal error
      description: Generates an internal error due to a wrong array declaration
      responses:
        500:
          description: Internal Error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/problem"
      x-amples:
        - title: wrong array declaration example
          request: {}
          response:
            status: 500
            headers:
              content-type: application/json
  /ex/err/file:
    get:
      tags:
        - Example
        - Error
        - Internal error
      description: Generates an internal error due to a non-existing file
      responses:
        500:
          description: Internal Error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/problem"
      x-amples:
        - title: non-existing file example
          request: {}
          response:
            status: 500
            headers:
              content-type: application/json
  /ex/err/manual/error:
    get:
      tags:
        - Example
        - Error
        - Internal error
      description: Generates an internal error due to a user-thrown error
      responses:
        500:
          description: Internal Error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/problem"
      x-amples:
        - title: user error example
          request: {}
          response:
            status: 500
            headers:
              content-type: application/json
  /ex/err/manual/deny:
    get:
      tags:
        - Example
        - Error
        - Access denied
      description: Generates an access-denied error
      responses:
        403:
          description: Access Denied
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/problem"
      x-amples:
        - title: access denied error example
          request: {}
          response:
            status: 403
            headers:
              content-type: application/json
  /ex/err/manual/auth:
    get:
      tags:
        - Example
        - Error
        - Unauthorised access
      description: Generates an unauthorised error
      responses:
        401:
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/problem"
      x-amples:
        - title: unauthorised error example
          request: {}
          response:
            status: 401
            headers:
              content-type: application/json
  /ex/req/uri/{uri}:
    get:
      tags:
        - Example
        - Request issuing
      description: Issues a request to the given URI
      responses:
        200:
          description: OK
      x-amples:
        - title: Get example home page
          request:
            params:
              uri: "http://www.example.com"

components:
  schemas:
    # A https://tools.ietf.org/html/draft-nottingham-http-problem
    problem:
      required:
        - type
      properties:
        status:
          type: integer
        type:
          type: string
        title:
          type: string
        detail:
          type: string
        method:
          type: string
        uri:
          type: string
  parameters:
    domain:
      in: path
      name: domain
      required: true
      schema:
        type: string
      description: |
        Project domain for the requested data.
    title:
      in: path
      name: title
      required: true
      schema:
        type: string
      description: |
        Page title. Use underscores instead of spaces. Example: `Main_Page`
    prop:
      in: path
      name: prop
      required: true
      schema:
        type: string
      description: |
        Site info prop.

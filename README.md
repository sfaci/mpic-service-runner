# Metrics Platform Instrument Configurator (MPIC)

Metrics Platform Instrument Configuration service

## Requirements

* Node.js 18
* Install [fresh](https://gerrit.wikimedia.org/g/fresh)

## Getting Started

Create a `config.yaml` local file just creating a symlink to `config.dev.yaml`

```
ln -s config.dev.yaml config.yaml`
```

Install the dependencies for backend and frontend:

```
npm install
npm --prefix frontend install
```

Build the frontend:

```
npm run build-frontend
```

### Development environment

There is a `docker-compose.dev.yaml` file to build a container that will run a development database with some data already
included. If you want to use it while developing, just run:

```
docker compose -f docker-compose.dev.yaml up -d
```

And you will have a MariaDB container running and listening to connections on the port 3306. The file `.env` contains the values
that are been used to create and configure the database and tables.

### Running the service

To start the server locally simply run:

```
node server.js
```

You can also create your own `config.local.yaml` file just creating a copy of `config.dev.yaml` to customize it according
to your local environment (this filename is already included at `.gitignore`). That case, you can run the server running:

```
node server.js --config config.local.yaml
```

# Available endpoints

In `routes/mpic.js` file:

* GET `http://localhost:8080`: You'll see the login page and a navigation bar to explore the whole tool
* POST `http://localhost:8080/instruments`: Registers a new instrument
* GET 'http://localhost:8080/api/v1/instruments': Returns an array that contains the JSON representation of all registered instruments

### Docker

In order to generate the Docker image, the blubber file needs to be configured and [Buidkit](https://github.com/moby/buildkit) must be installed.

To create the Dockerfile (for development purposes):
```
docker build --target development -f .pipeline/blubber.yaml -t mpic .
```

You can change the target name in the above command to build the image according to any existing `variant` in the blubber file (for example: development, test or production)

To run the service as a docker container locally for the first time, run the following command:

```
docker run -d -p 8080:8080 --name mpic mpic
```

### Tests

To fire the test suite up (and see the test coverage), simply run:

```
npm test
```

You can also build a docker image and run tests inside a docker container:

To build the image:

```
docker build --target test -f .pipeline/blubber.yaml -t mpic-test .
```

To run the container for the first time:

```
docker run --name mpic-test mpic-test
```

### Troubleshooting

In a lot of cases when there is an issue with node it helps to recreate the
`node_modules` directory:

```
rm -r node_modules
fresh-node
npm install
```

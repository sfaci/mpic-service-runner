import { createMemoryHistory, createRouter } from 'vue-router'

import Login from './components/Login.vue'
import List from './components/List.vue'
import Launch from './components/Launch.vue'
import Template from './components/Template.vue'
import Update from './components/Update.vue'
import Expire from './components/Expire.vue'

const routes = [
  { path: '/', component: Login },
  { path: '/list', component: List },
  { path: '/launch', component: Launch },
  { path: '/template', component: Template },
  { path: '/update', component: Update },
  { path: '/expire', component: Expire },
]

const router = createRouter({
  history: createMemoryHistory(),
  routes,
})

export default router

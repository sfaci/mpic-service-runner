'use strict';

const preq   = require('preq');
const assert = require('../../utils/assert.js');
const Server = require('../../utils/server.js');

describe('JSON instrument list works', function () {

	this.timeout(20000);

	let uri = null;
	const server = new Server();

	before(() => {
		return server.start()
			.then(() => {
				uri = `${server.config.uri}api/v1/instruments`;
			});
	});

	after(() => server.stop());

	it('instrument list should be an array of instruments', () => {
		return preq.get({
			uri
		}).then((res) => {
			assert.status(res, 200);
			assert.contentType(res, 'application/json');
			assert.notDeepEqual(res.body, undefined, 'No body returned!');
			assert.isArray(res.body);
		});
	});

	it('should throw a 404 for a non-existent endpoint', () => {
		return preq.get({
			uri: `${server.config.uri}api/v1/instrument/sampleInstrument`
		}).then((res) => {
			throw new Error('Expected an error to be thrown, got status: ', res.status);
		}, (err) => {
			assert.deepEqual(err.status, 404);
		});
	});
});

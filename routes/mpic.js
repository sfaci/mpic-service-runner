'use strict';

const BBPromise = require('bluebird');
const sUtil = require('../lib/util');
const HTTPError = sUtil.HTTPError;
const { body, validationResult } = require('express-validator');
const { addInstrument, getInstrument, getInstruments } = require('../controllers/instrumentController.js');
const path = require('path');

/**
 * The main router object
 */
const router = sUtil.router();
const fs = BBPromise.promisifyAll(require('fs'));
let app;

router.get('/', (req, res) => {
	res.redirect('/instruments');
});

router.get('/instruments', (req, res) => {
    getInstruments(req, res);
});

// No auth required for this endpoint
router.get('/api/v1/instruments', (req, res) => {
    getInstruments(req, res);
});

router.get('/instrument/:slug', (req, res) => {
    getInstrument(req, res);
});

router.post('/instruments',
	[
		[
			body('instrumentName').notEmpty().trim(),
			body('slug').notEmpty().trim(),
			body('description').trim(),
			body('owner').notEmpty().trim(),
			body('purpose').trim(),
			body('startDate').notEmpty().trim(),
			body('durationAmount').notEmpty().isNumeric(),
			body('durationTime').notEmpty().trim(),
			// body('task').notEmpty().trim(),
			body('complianceRequirements').notEmpty().trim(),
			body('sampleUnit').notEmpty().trim(),
			body('sampleRate').notEmpty().trim(),
			body('contextualAttributes').notEmpty().trim(),
			body('environments').notEmpty().trim()
		]
	],
	(req, res) => {
		// TODO Validate the double-submit cookie

		// Form validation
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			req.logger.log('error', 'There are some errors while validating the form');
			return res.status(400).json({ errors: errors.array() });
		}

		addInstrument(req, res);
	});

module.exports = (appObj) => {

	app = appObj;

	return {
		path: '/',
		skip_domain: true,
		router
	};
};

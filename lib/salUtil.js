'use strict';

function getNewSalEntry(creator, instrumentName) {
	const now = new Date();
	return '\n* ' + now.getHours().toString().padStart(2, '0') + ':' + now.getMinutes().toString().padStart(2, '0')
	+ ' ' + creator + ': A new instrument has been created - ' + instrumentName
}

function getCurrentSalHeader() {
	const now = new Date();
	return '\n===' + now.toISOString().split('T')[0] + '===\n';
}

function getPageContent(response) {
	let content = response.query.pages;
	content = content[Object.keys(content)[0]];
	return content.revisions[0].slots.main['*'];
}

module.exports = {
	getNewSalEntry,
	getCurrentSalHeader,
	getPageContent
}
